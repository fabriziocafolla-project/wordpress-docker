#!/bin/bash

source /usr/local/sbin/base.sh

main(){
	service nginx start
	service php7.4-fpm start

	/bin/bash
}

main $@
