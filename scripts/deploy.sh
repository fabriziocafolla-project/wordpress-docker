#!/bin/bash

set -feE -o functrace

failure() {
  local lineno=$1
  local msg=$2
  echo "Failed at $lineno: $msg"
}
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

set -o pipefail

parser(){
    options=$(getopt -l "help,deploy-server:,deploy-user:,ci-registry:,ci-registry-user:,ci-registry-image:,appname:" -o "h" -a -- "$@")
    eval set -- "$options"

    while true
       do
        case $1 in
            -h|--help)
                showHelp
                ;;
            --deploy-server)
                DEPLOY_SERVER=$2
                ;;
            --deploy-user)
                DEPLOY_USER=$2
                ;;
            --ci-registry)
                CI_REGISTRY=$2
                ;;
            --ci-registry-user)
                CI_REGISTRY_USER=$2
                ;;
            --ci-registry-image)
                CI_REGISTRY_IMAGE=$2
                ;;
            --appname)
                APPNAME=$2
                ;;
            --)
                shift
                break;;
        esac
        shift
    done

    shift "$(($OPTIND -1))"

  #IP SERVERS
  if [ -z $DEPLOY_SERVER ] ; then
    read -p 'DEPLOY_SERVER: ' DEPLOY_SERVER
  fi
  [[ "$DEPLOY_SERVER" != "" ]] || (echo 'DEPLOY_SERVER not valid' && exit 1)

  if [ -z $DEPLOY_USER ] ; then
    read -p 'DEPLOY_USER: ' DEPLOY_USER
  fi
  [[ "$DEPLOY_USER" != "" ]] || (echo 'DEPLOY_USER not valid' && exit 1)

  if [ -z $CI_REGISTRY ] ; then
    read -p 'CI_REGISTRY: ' CI_REGISTRY
  fi
  [[ "$CI_REGISTRY" != "" ]] || (echo 'CI_REGISTRY not valid' && exit 1)

  if [ -z $CI_REGISTRY_USER ] ; then
    read -p 'CI_REGISTRY_USER: ' CI_REGISTRY_USER
  fi
  [[ "$CI_REGISTRY_USER" != "" ]] || (echo 'CI_REGISTRY_USER not valid' && exit 1)

  if [ -z $CI_REGISTRY_IMAGE ] ; then
    read -p 'CI_REGISTRY_IMAGE: ' CI_REGISTRY_IMAGE
  fi
  [[ "$CI_REGISTRY_IMAGE" != "" ]] || (echo 'CI_REGISTRY_IMAGE not valid' && exit 1)

  if [ -z $APPNAME ] ; then
    read -p 'APPNAME: ' APPNAME
  fi
  [[ "$APPNAME" != "" ]] || (echo 'APPNAME not valid' && exit 1)
}

main() {

  parser $@

  servers=(${DEPLOY_SERVER//,/ })

  for server in "${servers[@]}"
  do
    ssh -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no $DEPLOY_USER@$server <<ENDSSH 
      docker login ${CI_REGISTRY} -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
      #docker rm -f $APPNAME
      #docker run -d -t -p 80:80 -p 465:465 -p 443:443 --name $APPNAME $CI_REGISTRY_IMAGE
ENDSSH
  done
}

main $@