#!/bin/bash

set -eE -o functrace

failure() {
  local lineno=$1
  local msg=$2
  echo "Failed at $lineno: $msg"
}
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

set -o pipefail

ENV_VALIDATOR=("dev" "pro")
FILE_ENV="./.env"

parser(){
    options=$(getopt -l "help,env:,appname:,domain:,skip-download" -o "h e: a: d: s" -a -- "$@")
    eval set -- "$options"

    while true
       do
        case $1 in
            -h|--help)
                showHelp
                ;;
            -e|--env)
                ENV=$2
                ;;
            -a|--appname)
                APPNAME=$2
                ;;
            -d|--domain)
                DOMAIN=$2
                ;;
            -s|--skip-download)
                SKIP_DOWNLOAD=true
                ;;
            --)
                shift
                break;;
        esac
        shift
    done

    shift "$(($OPTIND -1))"

  if [ -z $ENV ] ; then
    read -p 'Env, digit dev or pro: ' ENV
  fi
  [[ ${ENV_VALIDATOR[@]} =~ (^|[[:space:]])$ENV($|[[:space:]]) ]] || (echo "ENV not valid" && exit 1) 
  
  if [ -z $APPNAME ] ; then
    read -p 'App name: ' APPNAME
  fi
  [[ "$APPNAME" != "" ]] || (echo 'APPNAME not valid' && exit 1)

  if [ -z $DOMAIN ] ; then 
    if [ $ENV == "dev" ] ; then
      DOMAIN="$APPNAME.local"
    else
      read -p 'Domain: ' DOMAIN
    fi
  fi
  [[ "$DOMAIN" != "" ]] || (echo 'DOMAIN not valid' && exit 1)
}

update_env() {
  local _file=${1}
  local _key=${2}
  local _value=${3}
  echo "${_key}=${_value}" >> ${_file}
}


set_dev_env() {
  if [ "$ENV" != "dev" ] ; then
    return
  fi

  read -p 'SSH key (enter to skip): ' SSHKEY
  if [ "${SSHKEY}" == "" ] ; then 
    echo "#SETTING DEV SKIPPED" >> $FILE_ENV
    exit 0
  fi 

  if [ -f ${SSHKEY} ] ; then
    update_env $FILE_ENV "SSHKEY" $SSHKEY
  else
    echo "${SSHKEY} non trovata"
    exit 1
  fi
  
  read -p 'USER SERVER (root or sudo user): ' SRV_USER
  update_env $FILE_ENV "SRV_USER" $SRV_USER
    
  read -p 'USER SERVER WORKDIR: ' SRV_USER_WK
  if [ "${SRV_USER_WK}" == "" ] ; then
    $SRV_USER_WK=$SRV_USER
  fi
  update_env $FILE_ENV "SRV_USER_WK" $SRV_USER_WK

  read -p 'AWS IP EC2: ' SRV_HOST
  update_env $FILE_ENV "SRV_HOST" $SRV_HOST

  if [ -z $SKIP_DOWNLOAD ] ; then
    
    if [ -d "./wordpress" ] ; then
      echo "skip download folder wordpress exist."
      return
    fi

    curl -O https://wordpress.org/latest.zip
    
    unzip latest.zip

    rm latest.zip

    if [ ! -d "./wordpress" ] ; then
      echo "[ERROR] wordpress not unziped"
    fi

  cat << EOF  > ./wordpress/.gitignore
# ignore everything in the root except the "wp-content" directory.
!wp-content/

# ignore everything in the "wp-content" directory, except:
# "mu-plugins", "plugins", "themes" directory
wp-content/*
!wp-content/mu-plugins/
!wp-content/plugins/
!wp-content/themes/

# ignore these plugins
wp-content/plugins/hello.php

# ignore node dependency directories
node_modules/

# ignore log files and databases
*.log
*.sql
*.sqlite
EOF
  fi
}

set_env() {
  touch $FILE_ENV

  update_env $FILE_ENV "ENV" "${ENV}"
  update_env $FILE_ENV "APPNAME" "${APPNAME}"
  update_env $FILE_ENV "DOMAIN" "${DOMAIN}"
  update_env $FILE_ENV "DOCKERFILE_PATH" "."
  update_env $FILE_ENV "WORKDIR_USER" "www-data"
  update_env $FILE_ENV "WORKDIR_GROUP" "www-data"
  update_env $FILE_ENV "WORKDIRPATH" "/var/www/${APPNAME}"
  update_env $FILE_ENV "SOURCEPATH" "./wordpress"
  update_env $FILE_ENV "CONTAINERPATH" "./container"
  update_env $FILE_ENV "VOLUMESPATH" "./container/data"
}

main() {
  if [ -f "$FILE_ENV" ] ; then
    echo "[ERROR] file .env exist"
    exit 1
  fi

  parser $@

  set_env

  set_dev_env

  exit 0
}

main $@
