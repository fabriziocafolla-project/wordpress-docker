#!/bin/bash

set -feE -o functrace

failure() {
  local lineno=$1
  local msg=$2
  echo "Failed at $lineno: $msg"
}
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

set -o pipefail


parser(){
    options=$(getopt -l "help,ci-registry-image:" -o "h" -a -- "$@")
    eval set -- "$options"

    while true
       do
        case $1 in
            -h|--help)
                showHelp
                ;;
            --ci-registry-image)
                CI_REGISTRY_IMAGE=$2
                ;;
            --)
                shift
                break;;
        esac
        shift
    done

    shift "$(($OPTIND -1))"

  if [ -z $CI_REGISTRY_IMAGE ] ; then
    read -p 'CI_REGISTRY_IMAGE: ' CI_REGISTRY_IMAGE
  fi
  [[ "$CI_REGISTRY_IMAGE" != "" ]] || (echo 'CI_REGISTRY_IMAGE not valid' && exit 1)
}


main() {
  parser $@

  docker push $CI_REGISTRY_IMAGE
}

main $@