#!/bin/bash

set -feE -o functrace

failure() {
  local lineno=$1
  local msg=$2
  echo "Failed at $lineno: $msg"
}
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

set -o pipefail

source ./.env

parser(){
    options=$(getopt -l "help,ci-registry-image:,ci-target:" -o "h" -a -- "$@")
    eval set -- "$options"

    while true
       do
        case $1 in
            -h|--help)
                showHelp
                ;;
            --ci-registry-image)
                CI_REGISTRY_IMAGE=$2
                ;;
            --ci-target)
                CI_TARGET=$2
                ;;
            --)
                shift
                break;;
        esac
        shift
    done

    shift "$(($OPTIND -1))"

  if [ -z $CI_REGISTRY_IMAGE ] ; then
    read -p 'CI_REGISTRY_IMAGE: ' CI_REGISTRY_IMAGE
  fi
  [[ "$CI_REGISTRY_IMAGE" != "" ]] || (echo 'CI_REGISTRY_IMAGE not valid' && exit 1)

  if [ -z $CI_TARGET ] ; then
    read -p 'CI_TARGET: ' CI_TARGET
  fi
  [[ "$CI_TARGET" != "" ]] || (echo 'CI_TARGET not valid' && exit 1)
}


main() {
  parser $@
  
  docker build --tag "$CI_REGISTRY_IMAGE:latest" \
		--target="$CI_TARGET" \
		--quiet \
		--no-cache \
		--build-arg ENV=$ENV \
		--build-arg APPNAME=$APPNAME \
		--build-arg DOMAIN=$DOMAIN \
		--build-arg WORKDIR_USER=$WORKDIR_USER \
		--build-arg WORKDIR_GROUP=$WORKDIR_GROUP \
		--build-arg WORKDIRPATH=$WORKDIRPATH \
		$DOCKERFILE_PATH
}

main $@
