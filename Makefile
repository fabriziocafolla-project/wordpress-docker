.PHONY: help

help: ## helper
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.DEFAULT_GOAL := help

CONF ?= ./.env
VERSION = "1.0"

version: ## version
	@echo ${VERSION}
##

ifeq ($(shell test -e $(CONF) && echo -n yes),yes)

include $(CONF)
export $(shell sed 's/=.*//' $(CONF))

## ENV: DEV
ifeq ($(ENV),dev)
down: ## down containers
	@docker-compose down

up:  ## up -d containers
	@docker-compose up -d

build:  # build containers
	@docker-compose build

exec: ## entra nel container
	@docker exec -it $(APPNAME) /bin/bash

ssh_root: ## connessione ssh con l'utenza root o sudoers
	@ssh -i $(SSHKEY) $(SRV_USER)@$(SRV_HOST)

ssh: ## connessione ssh con l'utente proprietario della workdir
	@ssh -i $(SSHKEY) $(SRV_USER_WK)@$(SRV_HOST)

deploy: down build up ## rebuild containers

endif
##

## CI
ifeq ($(ENV),pro)
push: ## push immagine
	@./scripts/push.sh 

deploy: down build up

build: ## build immagine
	@./scripts/build.sh --ci-registry-image $(APPNAME) --ci-target $(ENV)

down: #pusblish REGISTRY is an input
	@docker-compose down

up:
	@docker-compose -f docker-compose.pro.yml up -d

exec:
	@docker exec -it $(APPNAME) /bin/bash 

endif
##

endif
##

## APP NON CONFIGURATA
ifneq ($(shell test -e $(CONF) && echo -n yes),yes)
APPNAME="none"
DOMAIN="none"
ENV="none"

setup: #first install
	@chmod +x ./scripts/*.sh
	@./scripts/setup.sh --env $(ENV) --appname $(APPNAME) --domain $(DOMAIN)

manual: ## manuale configurazione 
	@echo "read Readme.md"
	@echo "Missing .env file"
endif
##
