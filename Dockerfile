#BUILDER
ARG TAG="18.04"

FROM ubuntu:${TAG} AS builder

ARG ENV
ARG APPNAME
ARG DOMAIN 

RUN test -n "${ENV}" || (echo "[BUILD ARG] ENV not set" && false) 
RUN test -n "${APPNAME}" || (echo "[BUILD ARG] APPNAME not set" && false)
RUN test -n "${DOMAIN}" || (echo "[BUILD ARG] DOMAIN not set" && false)

ARG WORKDIR_USER=www-data
ARG WORKDIR_GROUP=www-data
ARG WORKDIRPATH=/var/www

RUN apt-get update && \
  apt-get install -y software-properties-common && \
  add-apt-repository ppa:ondrej/php && \
  apt-get upgrade -y && \
  apt-get install -y --no-install-recommends \
        vim \
        acl \
        ssh \
	nginx \
	dateutils \
	php7.4-fpm \
	php7.4-mbstring \
	php7.4-soap \
	php7.4-cli \
	php7.4-mysql \
	php7.4-gd \
	php7.4-imagick \
	php7.4-tidy \
	php7.4-xmlrpc \
	php7.4-xmlwriter \ 
	php7.4-xml \
	php7.4-curl \ 
	php7.4-dom \ 
	php7.4-zip \ 
    && apt-get -qy autoremove \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

COPY ./container/etc/nginx /etc/nginx
COPY ./container/etc/php/7.4 /etc/php/7.4
COPY ./container/sbin /usr/local/sbin

ENV ENV ${ENV}
ENV APPNAME ${APPNAME}
ENV DOMAIN ${DOMAIN}
ENV WORKDIR_USER ${WORKDIR_USER}
ENV WORKDIR_GROUP ${WORKDIR_GROUP}
ENV WORKDIRPATH ${WORKDIRPATH}

RUN chmod 744 -R /usr/local/sbin \
    && setfacl -R -m g:www-data:rwx /usr/local/sbin \
    && /usr/local/sbin/setup/nginx.sh \ 
    && /usr/local/sbin/setup/workdir.sh \
    && /usr/local/sbin/setup/create_cert.sh 

ENTRYPOINT ["/usr/local/sbin/services/init.sh"]

#Production container image
FROM builder AS pro 

#generated into build stage
COPY ./container/etc/cert /etc/cert/${APPNAME}

COPY ./wordpress ${WORKDIRPATH}

#Staging container image
FROM pro AS sta

#Develop container image
FROM builder AS dev

RUN apt-get update && \
  apt-get install -y --no-install-recommends \
	mysql-client \
	build-essential \ 
	iputils-ping \
    && apt-get -qy autoremove \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*
