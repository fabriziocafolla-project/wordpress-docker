# Wordpress

## Server

	1. Req. Ubuntu 18.04 or >

	2. Package: docker, docker-compose, bash, make

	3. SSH access

## DEV
	
	1. make setup ENV=dev APPNAME=yourname DOMAIN=yourdomain.local

	2. make deploy

	3. Wordpress config:
		db: 	appname
		user:	root
		pass:	admin
		host:	mysql.private

## PRO

	1. make setup ENV=pro APPNAME=yourname DOMAIN=yourdomain.xzy

	2. make deploy

## WORDPRES

	A. Di default gli uploads vengono ignorate da git: per far funzionare tutto in produzione usate un bucket e un CDN. ATTENZIONE se non fate così modificate il .gitignore oppure eseguite backup dei file.  

	B. wp-config. php viene ignorato per questioni di sicurezza

	C. Salvate all'interno della cartella /container/etc/cert i certificate ssl per il domini in produzione. ATTENZIONE AD USARE GLI STESSI NOMI DEI FILE ESISTENTI.


## AWS infrastruttura

**AWS POLICY** per i servizi necessari EC2, RDS, S3 e CDN. Ogni risorsa aws aggiungete un tag client=nome_client in modo da avere tutte le risorse riconoscibili per quell'utente 

	{
		"Version": "2012-10-17",
		"Statement": [
			{
				"Sid": "ec2",
				"Effect": "Allow",
				"Action": [
					"ec2:Describe*"
				],
				"Resource": "*",
				"Condition": {
					"StringEquals": {
						"aws:PrincipalTag/client": "${aws:username}"
					}
				}
			},
			{
				"Sid": "rds",
				"Effect": "Allow",
				"Action": [
					"rds:Describe*"
				],
				"Resource": "*",
				"Condition": {
					"StringEquals": {
						"aws:PrincipalTag/client": "${aws:username}"
					}
				}
			},
			{
				"Sid": "s3",
				"Effect": "Allow",
				"Action": [
					"s3:Get*",
					"s3:List*",
					"s3:Put*",
					"s3:DeleteObject"
				],
				"Resource": "*",
				"Condition": {
					"StringEquals": {
						"aws:PrincipalTag/client": "${aws:username}"
					}
				}
			},
			{
				"Sid": "cdn",
				"Effect": "Allow",
				"Action": [
					"cloudfront:List*",
					"cloudfront:Get*"
				],
				"Resource": "*",
				"Condition": {
					"StringEquals": {
						"aws:PrincipalTag/client": "${aws:username}"
					}
				}
			}
		]
	}